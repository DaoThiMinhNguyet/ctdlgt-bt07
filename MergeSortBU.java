import java.util.Scanner;

/**
 * Created by Nguyet on 10/21/15.
 */
public class MergeSortBU {
    private MergeSortBU(){  }
    public static void merge(String[] a, String[]aux, int lo, int mid, int hi){
        for(int k=lo; k<=hi; k++){
            aux[k] = a[k];
        }
        int i=lo, j=mid+1;
        for(int k=lo; k<=hi; k++){
            if(i>mid)         a[k]=aux[j++];
            else if(j>hi)     a[k]=aux[i++];
            else if(less(aux[j], aux[i]))  a[k]=aux[j++];
            else                  a[k]=aux[i++];
        }
    }
    public static void sort(String[]a){
        int N=a.length;
        String[]aux=new String[N];
        for(int n=1; n<N; n=n+n){
            for(int i=0; i<N-n; i+=n+n){
                int lo=i;
                int m=i+n-1;
                int hi=Math.min(i+n+n-1, N-1);
                merge(a, aux, lo, m, hi);
            }
        }
        assert isSorted(a);
    }
  /*  private static void exch(Object[]a, int i, int j){
        Object swap = a[i];
        a[i]=a[j];
        a[j]=swap;
    }*/
    private static boolean isSorted(String[]a){
        for(int i=1; i<a.length; i++)
            if(less(a[i],a[i-1])) return false;
        return true;
    }
    public static boolean less(String v, String w){
        return v.compareTo(w)<0;
    }
    private static void show(String[]a){
        for(int i=0; i<a.length; i++){
            System.out.println(a[i] +" ");
        }
    }
    public static void main(String[] args){
        Scanner input=new Scanner(System.in);
        String[]a;
        int lo, hi, n;
        System.out.println("nhap so luong ky tu cua day:");
        n=input.nextInt();
        a=new String[n];
        System.out.println("day can sap xep la:");
        for(int i=0; i<n; i++)
            a[i]=input.nextLine();
        System.out.println("day da nhap la:");
        for(int i=0; i<a.length; i++)
            System.out.println(a[i] +" ");
        MergeSortBU.sort(a);
        System.out.println();
        System.out.println("day da duoc sap xep la:");
        show(a);
    }
}
