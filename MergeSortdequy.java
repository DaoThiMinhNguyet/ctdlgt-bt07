import java.util.Scanner;

/**
 * Created by Nguyet on 10/21/15.
 */
public class MergeSortdequy {
    public MergeSortdequy(){ }
    public static void merge(int[]a, int[]aux, int lo, int mid, int hi){
        for(int k=lo; k<=hi; k++){
            aux[k]=a[k];
        }
        int i=lo, j=mid+1;
        for(int k=lo; k<=hi; k++){
            if(i>mid)                a[k]=aux[j++];
            else if(j>hi)            a[k]=aux[j++];
            else if(aux[j]<aux[i])   a[k]=aux[j++];
            else                     a[k]=aux[i++];
        }
    }
    public static void MergeSort(int[]a, int lo, int hi){
        int N=a.length;
        int[]aux=new int[N];
        if(hi<=lo)    return;
        int mid=lo+(hi-lo)/2;
        MergeSort(a, lo, hi);
        MergeSort(a,mid+1, hi);
        merge(a,aux,lo, mid, hi);
    }
    public static boolean isSorted(int[]a){
        for(int i=1; i<a.length; i++)
            if(a[i]<a[i-1])   return false;
            return true;
    }
    public static void show(int[]a){

        for(int i=0; i<a.length; i++){
            System.out.print(a[i]+" ");
        }
    }
    public static void main(String[]args){
        Scanner input=new Scanner(System.in);
        int[]a;
        int lo, hi, n;
        System.out.println("nhap n:");
        n=input.nextInt();
        a=new int[n];
        System.out.println("day can sap xep la:");
        for(int i=0; i<n; i++)
            a[i]=input.nextInt();
        System.out.println("day da nhap la:");
        for(int i=0; i<a.length; i++)
            System.out.println(a[i] +" ");
        MergeSortdequy.MergeSort(a, 0, n-1);
        System.out.println();
        System.out.println("day da duoc sap xep la:");
        show(a);
    }

}
